const moment = window.moment = require('moment-timezone');
moment.tz.setDefault("Europe/London");
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        baseURL: 'https://wowapi.metweb.ie/',
        source: '', // axios's CancelToken source, it is used in WowApi class
        map: {
            baseMap: '',
            view: '',
            markersLayer: '',
            showModal: false, //is fired when a marker is clicked on the homepage map
        }, //ol map object
        mapJsonData: '',
        registeredSitesData: '',
        includeRegisteredLocations: false, // for stations param in API call
        includeWowObservations: true, // for stations param in API call
        includeOfficialObservations: true, // for stations param in API call
        mapControls:{
            updateMap: true, // boolean value to listen if the map needs to be updated whenever a control is changed
            location: '',
            period: moment().format('DD/MM/YYYY'), // the time and date overlay on the map
            periodTime: moment().endOf('hour').format('HH:mm'),
            impactTimePoint: -1,
            impactFilter: '',
            element: 'dry_bulb', // default selected element
            stations: [
                    {slug:'showWowData', name: 'WOW Observations'},
                    {slug:'showOfficialData', name: 'Official Observations'}
                ]
        },
        selectedStation: {}, // this is assigned whenever a marker is clicked in home page & in single station page (includes site details)
        selectedStationObservations: {},
        stationTableData: [], // table tab of single station page
        stationGraphData: [], // graph data of single station page
        isStationTableDataLoaded: false, // just a flag to know if the station table (tab) data (under data display tab) has been loaded or not
        stationDataDisplay: {
            startDate: moment().subtract(1,'day'),
            startTime: moment().format('HH:mm'),
            endDate: moment(),
            endTime: moment().format('HH:mm'),
            filters: ['temperature']
        },
        stationsToCompare: [], // max 2 stations can be in this array - single station page
        updateSingleStationDataDisplay: false,

        // ENTER WEATHER IMPACT FORM
        typesOfWeatherEffects: [], // selected checkboxes (impact types)
        weatherImpactSubmitted: false,
        isWeatherImpactValid: false,
        enterWeatherImpact:{
            id: null, // Id of the weather impact report
            observationId: null, // If this impact is linked to an observation, its Id will show up here.
            siteId: null, // If this impact is linked to a site, its Id will show up here.
            imageId: null, // If this impact is linked to a site, its Id will show up here.
            elevation: 0.0, // Elevation / Height of the coordinates
            userId: null, // Sender user Id
            longitude: 0.0, // Longitude of the impact coordinate
            latitude: 0.0, // Latitude of the impact coordinate
            reportDate: moment().format('YYYY-MM-DDTHH:mm:ssZ'), // Impact report date
            weatherImpacts: [] // includes all the details of the selected impact type, its severity and hazards
        },
        // END - ENTER WEATHER IMPACT FORM

    
    },
    getters:{
        mapNeedsUpdate: state => state.mapControls.updateMap,
        hasElementChanged: state => state.mapControls.element,
        hasPeriodChanged: state => state.mapControls.period + ', ' + moment(state.mapControls.periodTime,'HH:mm').startOf('hour').format('HH:mm') + ' to ' + state.mapControls.periodTime,
        hasLocationChanged: state => state.mapControls.location,
        getShowModal: state => state.map.showModal,
        singleStationLongLat : state => [state.selectedStation.longitude, state.selectedStation.latitude],
        impactLongLat : state => [state.selectedImpactDetails.longitude, state.selectedImpactDetails.latitude],
        singleStationDataDisplayNeedsUpdate: state => state.updateSingleStationDataDisplay,
        getImpactNameById: state => (id) => state.impactTypes.find( impact => impact.id === id ).name,
        getSeverityNameById: state => (id) => state.severityOptions.find(  severity => severity.id === id ).name,
        getComparedSiteById: state => (id) => state.stationsToCompare.find( element => element.id === id ? element : ''),
        getAllSitesInOne: state => state.mapJsonData.features.concat(state.registeredSitesData.features),
        getSiteById: (state, getters) => (idType, id) => getters.getAllSitesInOne.find(obj => obj.properties[idType] === id), // looks for also in the registered sites
        getStationById: state => (id) => state.mapJsonData.features.find(obj => obj.properties.siteId === id), //does not look for in the registered sites
        getWeatherImpactByType: state => (type) => state.enterWeatherImpact.weatherImpacts.filter(object => (object.type === type)),
        getIsStationTableDataLoadedStatus: state => state.isStationTableDataLoaded
    },
    mutations: {
        setIsStationTableDataLoadedStatus(state, status){ state.isStationTableDataLoaded = status},
        setShowModalStatus(state, status){ state.showModal = status}
    },
    actions: {

    }
  })
export default store;